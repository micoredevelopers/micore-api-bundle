<?php


namespace MiCore\ApiBundle\EventSubscriber;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class JsonToArraySubscriber implements EventSubscriberInterface
{

    public function toJson(ControllerEvent $event)
    {
        $request = $event->getRequest();

        if ($request->getContentType() !== 'json' || !$request->getContent()) {
            return;
        }
        try {
            $data = json_decode($request->getContent(), true);
            $request->request->replace(is_array($data) ? $data : array());
        } catch (\Exception $exception){
            throw new BadRequestHttpException('invalid json body: ' . json_last_error_msg());
        }
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
       return [
           KernelEvents::CONTROLLER => 'toJson',
       ];
    }
}
