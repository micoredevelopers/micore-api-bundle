<?php


namespace MiCore\ApiBundle\EventSubscriber;

use MiCore\ApiBundle\Api\ApiService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiExceptionSubscriber implements EventSubscriberInterface
{

    /**
     * @var \MiCore\ApiBundle\Api\ApiService
     */
    private $api;

    public function __construct(ApiService $api)
    {

        $this->api = $api;
    }

    public function onException(ExceptionEvent $event)
    {

        $request = $event->getRequest();
        if ($request->getContentType() !== 'json' || !$request->getContent()) {
            return;
        }

        $responseBuilder = $this->api->createResponseBuilder();
        $responseBuilder->setStatus(500)->setMsg('Server error!');


        $exception =  method_exists($event, 'getException') ? $event->getException() : $event->getThrowable();

        if ($exception instanceof NotFoundHttpException){
            $responseBuilder
                ->setStatus(404)
                ->setMsg($exception->getMessage());
        } else if ('dev' === $_ENV['APP_ENV']){
            dd($exception);
        }

        $event->setResponse($responseBuilder->createResponse());
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents()
    {
        return [
            'kernel.exception' => ['onException']
        ];
    }
}
