<?php


namespace MiCore\ApiBundle\DependencyInjection;


use MiCore\ApiBundle\Api\ApiService;
use MiCore\ApiBundle\Api\DataFormat\ApiResponseDataFormat;
use MiCore\ApiBundle\Api\DataFormat\ApiResponseDataFormatInterface;
use MiCore\ApiBundle\EventSubscriber\ApiExceptionSubscriber;
use MiCore\ApiBundle\EventSubscriber\JsonToArraySubscriber;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

class MiCoreApiExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container)
    {
        $container->autowire(ApiService::class);
        $container->autowire(ApiExceptionSubscriber::class)->addTag('kernel.event_subscriber');
        $container->autowire(JsonToArraySubscriber::class)->addTag('kernel.event_subscriber');
        $container->autowire(ApiResponseDataFormatInterface::class, ApiResponseDataFormat::class);

        if (isset($_ENV['APP_ENV']) && 'test' == $_ENV['APP_ENV']){

            foreach ($container->getDefinitions() as $id=>$definition){
                $definition->setPublic(true);
            }
        }
    }
}
