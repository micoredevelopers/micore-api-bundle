<?php


namespace MiCore\ApiBundle\Api\DataFormat;


use MiCore\ApiBundle\Api\ApiResponse;

interface ApiResponseDataFormatInterface
{

    /**
     * @param ApiResponse $apiResponse
     * @return mixed
     */
    public function format(ApiResponse $apiResponse);

}
