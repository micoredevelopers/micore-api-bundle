<?php


namespace MiCore\ApiBundle\Api\DataFormat;


use MiCore\ApiBundle\Api\ApiResponse;
use MiCore\ApiBundle\Api\ApiResponseList;
use Symfony\Contracts\Translation\TranslatorInterface;

class ApiResponseDataFormat implements ApiResponseDataFormatInterface
{

    /**
     * @var TranslatorInterface
     */
    private $translator;

    public function __construct(string $transDomain = 'validators', TranslatorInterface $translator = null)
    {
        $this->translator = $translator;
    }

    /**
     * @param ApiResponse $apiResponse
     * @return array|mixed
     */
    public function format(ApiResponse $apiResponse)
    {
        if ($apiResponse instanceof ApiResponseList){
           $data = [
               'rows' => $apiResponse->getData(),
               'count' => $apiResponse->getQ(),
               'limit' => $apiResponse->getLimit(),
               'page' => $apiResponse->getPage()
           ];

        } else {
            $data = $apiResponse->getData();
        }

        $errors = [];
        foreach ($apiResponse->getErrors() as $error){
            $msg = $this->translator ? $this->translator->trans($error->getMsg(), [], 'validators') : $error->getMsg();
            $propertyName = $this->translator ? $this->translator->trans($error->getPropertyPath(), [], 'validators') : $error->getPropertyPath();

            $errors[] = [
                'msg' => $msg,
                'propertyPath' => $error->getPropertyPath(),
                'frontError' => $propertyName.': '.$msg
            ];
        }

        return [
            'msg' => $apiResponse->getMsg(),
            'data' => $data,
            'errors' => $errors
        ];
    }
}
