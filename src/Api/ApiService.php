<?php


namespace MiCore\ApiBundle\Api;

use MiCore\ApiBundle\Api\DataFormat\ApiResponseDataFormatInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ApiService
{

    const DEFAULT_FORMAT = 'json';

    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var ApiResponseDataFormatInterface
     */
    private $apiResponseDataFormat;

    public function __construct(SerializerInterface $serializer, ApiResponseDataFormatInterface $apiResponseDataFormat)
    {
        $this->serializer = $serializer;
        $this->apiResponseDataFormat = $apiResponseDataFormat;
    }

    /**
     * @return ApiResponseBuilder
     */
    public function createResponseBuilder(): ApiResponseBuilder
    {
        return new ApiResponseBuilder($this);
    }

    /**
     * @param ApiResponse $responseData
     * @param string $format
     * @return Response
     */
    public function createResponse(ApiResponse $responseData, string $format = self::DEFAULT_FORMAT): Response
    {

        $resultData = $this->apiResponseDataFormat->format($responseData);
        $content = $this->serializer->serialize($resultData, $format, $responseData->getNormalizerContext());

        switch ($format) {
            case 'json':
                $headers['Content-Type'] = 'application/json';
                break;
        }

        return new Response($content, $responseData->getStatus(), $headers ?? []);
    }
}
