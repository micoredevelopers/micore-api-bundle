<?php


namespace MiCore\ApiBundle\Api;


use MiCore\ApiBundle\Api\Error\ErrorsIterator;
use MiCore\ApiBundle\Api\Error\ErrorsIteratorInterface;

class ApiResponse
{

    /**
     * @var integer
     */
    protected $status = 200;

    /**
     * @var string|null
     */
    protected $msg;

    /**
     * @var mixed
     */
    protected $data;

    /**
     * @var ErrorsIteratorInterface
     */
    protected $errors;

    /**
     * @var array
     */
    protected $normalizerContext = [];

    public function __construct()
    {
        $this->errors = new ErrorsIterator();
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * @param $msg
     * @return $this
     */
    public function setMsg($msg): self
    {
        $this->msg = $msg;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data): self
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return ErrorsIteratorInterface
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param string $msg
     * @param string $propertyPath
     * @return $this
     */
    public function addError(string $msg, string $propertyPath): self
    {
        $this->errors->addError($msg,  $propertyPath);
        return $this;
    }

    /**
     * @return array
     */
    public function getNormalizerContext()
    {
        return $this->normalizerContext;
    }

    /**
     * @param $normalizerContext
     * @return $this
     */
    public function setNormalizerContext($normalizerContext): self
    {
        $this->normalizerContext = $normalizerContext;
        return $this;
    }

    public function addNormalizerContext(string $key, $value): self
    {
        $this->normalizerContext[$key] = $value;
        return $this;
    }

}
