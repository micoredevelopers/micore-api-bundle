<?php


namespace MiCore\ApiBundle\Api;


use MiCore\ApiBundle\Api\Error\ErrorInterface;
use MiCore\DoctrineBundle\Repository\Pagination\PaginationInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ApiResponseBuilder extends ApiResponse
{

    /**
     * @var ApiService
     */
    private $api;

    public function __construct(ApiService $api)
    {
        $this->api = $api;
        $this->setMsg('Success')->setStatus(200);
        parent::__construct();
    }


    /**
     * @param FormInterface $form
     * @param bool $submittedRequired
     * @return $this
     */
    public function loadErrorsFromForm(FormInterface $form, bool $submittedRequired = true): self
    {

        if (true === $submittedRequired && false === $form->isSubmitted()) {
            $this->addError('Sent data is empty', ErrorInterface::ERROR_PROPERTY_PATH_ROOT);
        } else {
            $errors = [];
            $this->getErrorsFromForm($errors, $form);

            foreach ($errors as $property => $messages) {
                foreach ($messages as $msg) {
                    $this->addError($msg, $property);
                }
            }
        }

        return $this;
    }

    /**
     * @param $errors
     * @param FormInterface $form
     * @param string $name
     */
    private function getErrorsFromForm(&$errors, FormInterface $form, $name = ErrorInterface::ERROR_PROPERTY_PATH_ROOT)
    {
        foreach ($form->getErrors() as $error) {
            $errors[$name][$error->getMessage()] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                $this->getErrorsFromForm($errors, $childForm, $childForm->getName());
            }
        }
    }

    /**
     * @param ConstraintViolationListInterface $constraintViolationList
     * @return ApiResponseBuilder
     */
    public function loadErrorsFromConstraints(ConstraintViolationListInterface $constraintViolationList): self
    {
        foreach ($constraintViolationList as $item) {
            $this->addError($item->getMessage(), $item->getPropertyPath());
        }
        return $this;
    }

    /**
     * @param string $msg
     * @param string|null $property
     * @return $this
     */
    public function addError(string $msg, string $property = ErrorInterface::ERROR_PROPERTY_PATH_ROOT): ApiResponse
    {
        if ($this->status === 200) {
            $this->status = 422;
            $this->msg = 'Error!';
        }
       return  parent::addError($msg, $property);
    }


    /**
     * @param array $groups
     * @return $this
     */
    public function setNormalizerGroups(array $groups): self
    {
        $this->addNormalizerContext('groups', $groups);
        return $this;
    }

    /**
     * @param int|null $page
     * @param int|null $limit
     * @param int|null $q
     * @return Response
     */
    public function createListResponse(?int $page = 1, ?int $limit = 10, ?int $q = null): Response
    {


        $result = new ApiResponseList($page, $limit, $q);
        $result
            ->setData($this->data)
            ->setMsg($this->msg)
            ->setStatus($this->status)
            ->setNormalizerContext($this->normalizerContext);

        foreach ($this->errors as $error){
            $result->addError($error->getMsg(), $error->getPropertyPath());
        }

        return $this->api->createResponse($result);
    }

    /**
     * @param PaginationInterface $pagination
     * @return Response
     */
    public function createListResponseFromPagination(PaginationInterface $pagination): Response
    {
        return $this->createListResponse($pagination->getPage(), $pagination->getLimit(), $pagination->getQ());
    }

    /**
     * @param string $format
     * @return Response
     */
    public function createResponse(string $format = ApiService::DEFAULT_FORMAT): Response
    {

        $result = new ApiResponse();
        $result
            ->setData($this->data)
            ->setMsg($this->msg)
            ->setStatus($this->status)
            ->setNormalizerContext($this->normalizerContext);

        foreach ($this->errors as $error){
            $result->addError($error->getMsg(), $error->getPropertyPath());
        }

        return $this->api->createResponse($result, $format);
    }

}
