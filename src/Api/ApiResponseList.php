<?php


namespace MiCore\ApiBundle\Api;


use MiCore\DoctrineBundle\Repository\Pagination\PaginationInterface;

class ApiResponseList extends ApiResponse
{

    /**
     * @var int|null
     */
    private $page;

    /**
     * @var int
     */
    private $limit;

    /**
     * @var int
     */
    private $q;

    public function __construct(?int $page = 1, ?int $limit = 10, ?int $q = null)
    {
        parent::__construct();
        $this->q = $q;
        $this->page = $page;
        $this->limit = $limit;
    }

    /**
     * @return int|null
     */
    public function getPage(): ?int
    {
        return $this->page;
    }

    /**
     * @param int|null $page
     * @return ApiResponseList
     */
    public function setPage(?int $page): ApiResponseList
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit ?? $this->countData();
    }

    /**
     * @param int $limit
     * @return ApiResponseList
     */
    public function setLimit(?int $limit): ApiResponseList
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getQ(): ?int
    {
        return $this->q ?? $this->countData();
    }

    /**
     * @param int $q
     * @return ApiResponseList
     */
    public function setQ(int $q): ApiResponseList
    {
        $this->q = $q;
        return $this;
    }

    /**
     * @param PaginationInterface $pagination
     * @return $this
     */
    public function setPagination(PaginationInterface $pagination): self
    {
        $this->page = $pagination->getPage();
        $this->limit = $pagination->getLimit();
        $this->q = $pagination->getQ();
        return $this;
    }

    /**
     * @param PaginationInterface $pagination
     * @return static
     */
    public static function createFromPagination(PaginationInterface $pagination): self
    {
        return new self($pagination->getPage(), $pagination->getLimit(), $pagination->getQ());
    }

    /**
     * @return int|null
     */
    private function countData(): ?int
    {
        if (is_countable($this->data)){
            return count($this->data);
        } else if (is_iterable($this->data)){
            $count = 0;
            foreach ($this->data as $item){
                $count++;
            }
            return $count;
        }
        return null;
    }

}
