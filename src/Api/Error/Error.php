<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 06.06.19
 * Time: 0:23
 */

namespace MiCore\ApiBundle\Api\Error;


use MiCore\ApiBundle\Api\Error\ErrorInterface;
use Symfony\Component\Serializer\Annotation\Groups;

class Error implements ErrorInterface
{

    /**
     * @Groups({"safe"})
     * @var string|null
     */
    private $msg;


    /**
     * @Groups({"safe"})
     * @var string|null
     */
    private $propertyPath;

    public function __construct(string $msg = null, string $propertyPath = null)
    {
        $this->msg = $msg;
        $this->propertyPath = $propertyPath;
    }

    /**
     * @return null|string
     */
    public function getMsg(): ?string
    {
        return $this->msg;
    }

    /**
     * @param null|string $msg
     */
    public function setMsg(?string $msg): void
    {
        $this->msg = $msg;
    }

    /**
     * @return null|string
     */
    public function getPropertyPath(): ?string
    {
        return $this->propertyPath;
    }

    /**
     * @param null|string $propertyPath
     */
    public function setPropertyPath(?string $propertyPath): void
    {
        $this->propertyPath = $propertyPath;
    }



}
