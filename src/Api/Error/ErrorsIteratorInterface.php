<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 05.06.19
 * Time: 23:51
 */

namespace MiCore\ApiBundle\Api\Error;


use MiCore\ApiBundle\Api\Error\ErrorInterface;

interface ErrorsIteratorInterface extends \Iterator
{

    public function current(): ErrorInterface;

}
