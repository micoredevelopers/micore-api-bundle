<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 06.06.19
 * Time: 0:11
 */

namespace MiCore\ApiBundle\Api\Error;

use MiCore\ApiBundle\Api\Error\Error;
use MiCore\ApiBundle\Api\Error\ErrorInterface;
use MiCore\ApiBundle\Api\Error\ErrorsIteratorInterface;

class ErrorsIterator extends \ArrayIterator implements ErrorsIteratorInterface
{
    public function current(): ErrorInterface
    {
        return parent::current();
    }

    public function addError(string $msg, string $propertyPath)
    {
        $error = new Error();
        $error->setMsg($msg);
        $error->setPropertyPath($propertyPath);
        $this->append($error);
    }
}
