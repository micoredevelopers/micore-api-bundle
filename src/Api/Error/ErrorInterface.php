<?php
/**
 * Created by PhpStorm.
 * User: basilliyc
 * Date: 06.06.19
 * Time: 0:07
 */

namespace MiCore\ApiBundle\Api\Error;


interface ErrorInterface
{
    public const ERROR_PROPERTY_PATH_ROOT = 'root';

    /**
     * @return string|null
     */
    public function getMsg(): ?string;

    /**
     * @return string|null
     */
    public function getPropertyPath(): ?string;

}
