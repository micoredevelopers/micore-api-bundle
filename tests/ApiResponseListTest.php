<?php


namespace MiCore\ApiBundle\Tests;


use MiCore\ApiBundle\Api\ApiResponse;
use MiCore\ApiBundle\Api\ApiResponseList;
use MiCore\ApiBundle\Api\Error\Error;
use MiCore\DoctrineBundle\Repository\Pagination\Pagination;
use PHPUnit\Framework\TestCase;

class ApiResponseListTest extends TestCase
{

    public function testAddError()
    {
        $response = new ApiResponse();
        $response->addError('test', 'foo');

        $this->assertInstanceOf(Error::class, $response->getErrors()->current());
        $this->assertEquals('foo', $response->getErrors()->current()->getPropertyPath());
        $this->assertEquals('test', $response->getErrors()->current()->getMsg());
    }

    /**
     * @param ApiResponseList $apiResponse
     * @param $page
     * @param $limit
     * @param $q
     * @dataProvider dataProvider
     */
    public function testGetPageLimitQ(ApiResponseList $apiResponse, $page, $limit, $q)
    {
        $this->assertEquals($apiResponse->getQ(), $q);
        $this->assertEquals($apiResponse->getLimit(), $limit);
        $this->assertEquals($apiResponse->getPage(), $page);
    }

    public function dataProvider()
    {

        $data = ['el1', 'el2', 'el3'];

        $response1 = new ApiResponseList(1,10, 20);

        $response2 = new ApiResponseList(null, null, null); $response2->setData($data);

        $response3 = ApiResponseList::createFromPagination(new Pagination(3,10)); $response3->setData($data);
        return [
            [$response1, 1,10,20],
            [$response2, null, count($data), count($data)],
            [$response3, 3, 10, count($data)],
        ];
    }

}
