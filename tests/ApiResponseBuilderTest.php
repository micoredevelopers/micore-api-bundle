<?php


namespace MiCore\ApiBundle\Tests;


use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormFactoryInterface;

class ApiResponseBuilderTest extends ApiServiceTestCase
{

    public function testLoadErrorsFromForm()
    {
        $form = $this->formFactory()->createBuilder(FormType::class)->add('foo', TextType::class)->getForm();
        $data['foo'] = [];
        $form->submit($data);
        $responseBuilder = $this->apiService->createResponseBuilder();
        $responseBuilder->loadErrorsFromForm($form);

        $resp = json_decode($responseBuilder->createResponse()->getContent());

        $this->assertEquals('foo', $resp->errors[0]->propertyPath);
        $this->assertEquals('foo: '.$resp->errors[0]->msg, $resp->errors[0]->frontError);
    }

    private function formFactory(): FormFactoryInterface
    {
        return self::$container->get(FormFactoryInterface::class);
    }

}
