<?php


namespace MiCore\ApiBundle\Tests;


use MiCore\ApiBundle\Api\ApiResponse;
use MiCore\ApiBundle\Api\ApiResponseList;
use MiCore\DoctrineBundle\Repository\Pagination\Pagination;
use Symfony\Component\HttpFoundation\Response;

class ApiServiceTest extends ApiServiceTestCase
{

    public function testCreateResponse()
    {
        $api = $this->apiService;

        $data = new Fixtures\Entity\Entity();

        $apiResp = (new ApiResponse())->setData($data);

        $resp = $api->createResponse($apiResp);
        $this->assertInstanceOf(Response::class, $resp);
        $this->assertEquals(1, json_decode($resp->getContent(),true)['data']['foo']);
        $this->assertEquals(2, json_decode($resp->getContent(), true)['data']['bar']);

        $apiResp->setNormalizerContext(['groups' => ['bar']]);
        $resp2 = $api->createResponse($apiResp);
        $this->assertEquals(2, json_decode($resp2->getContent(), true)['data']['bar']);
        $this->assertArrayNotHasKey('foo', json_decode($resp2->getContent(), true)['data']);

        $apiResp2 = (new ApiResponseList())->setData([$data]);
        $apiResp2->setPagination(new Pagination(4,20));
        $resp3 = $api->createResponse($apiResp2);
        $this->assertEquals(1, json_decode($resp3->getContent(), true)['data']['count']);
    }

}
