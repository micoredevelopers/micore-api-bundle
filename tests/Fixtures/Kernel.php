<?php


namespace MiCore\ApiBundle\Tests\Fixtures;


use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use MiCore\ApiBundle\MiCoreApiBundle;
use MiCore\KernelTest\KernelTest;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;

class Kernel extends KernelTest
{

    protected function bundles(): array
    {
        return [
            MiCoreApiBundle::class,
         //   Form\\\\\\
        ];
    }

    public function configureContainer(ContainerBuilder $c)
    {
        $c->autowire(FormFactoryInterface::class, FormFactory::class)->setPublic(true);
    }
}
