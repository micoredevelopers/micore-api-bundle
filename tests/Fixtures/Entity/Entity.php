<?php


namespace MiCore\ApiBundle\Tests\Fixtures\Entity;


use Symfony\Component\Serializer\Annotation\Groups;

class Entity
{

    public $foo = 1;

    /**
     * @Groups("bar")
     * @var int
     */
    public $bar = 2;

}
