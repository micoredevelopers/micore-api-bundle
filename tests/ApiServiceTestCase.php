<?php


namespace MiCore\ApiBundle\Tests;


use Doctrine\ORM\EntityManagerInterface;
use MiCore\ApiBundle\Api\ApiService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ApiServiceTestCase extends KernelTestCase
{

    /**
     * @var ApiService
     */
    protected $apiService;

    protected function setUp(): void
    {
        self::bootKernel();
        $this->apiService = self::$container->get(ApiService::class);
    }

}
